import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NewProductForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: '',
      price: '',
      name: '',
      stocked: true,
    };

    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.onChangeStocked = this.onChangeStocked.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChangeCategory(e) {
    this.setState({ category: e.target.value });
  }

  onChangeName(e) {
    this.setState({ name: e.target.value });
  }

  onChangePrice(e) {
    this.setState({ price: e.target.value });
  }

  onChangeStocked(e) {
    this.setState({ stocked: e.target.checked });
  }

  handleSubmit(e) {
    e.preventDefault();
    const product = {
      category: this.state.category,
      price: this.state.price,
      name: this.state.name,
      stocked: this.state.stocked,
    };
    this.props.addNewProduct(product);
    this.setState({ category: '' });
    this.setState({ name: '' });
    this.setState({ price: '' });
    this.setState({ stocked: false });
  }

  render() {
    return (
      <div>
        <form id="new-product-form" onSubmit={this.handleSubmit}>
          <label>
            Category:
            <input
              type="text"
              name="category"
              onChange={this.onChangeCategory}
              value={this.state.category}
            />
          </label>
          <br />
          <label>
            Name:
            <input
              type="text"
              name="name"
              onChange={this.onChangeName}
              value={this.state.name}
            />
          </label>
          <br />
          <label>
            Price:
            <input
              type="text"
              name="price"
              onChange={this.onChangePrice}
              value={this.state.price}
            />
          </label>
          <br />
          <label>
            In Stock:
            <input
              type="checkbox"
              name="stocked"
              onChange={this.onChangeStocked}
              value={this.state.stocked}
            />
          </label>
          <br />
          <br />
          <input type="submit" />
        </form>
      </div>
    );
  }
}

NewProductForm.propTypes = {
  addNewProduct: PropTypes.func.isRequired,
};

export default NewProductForm;
