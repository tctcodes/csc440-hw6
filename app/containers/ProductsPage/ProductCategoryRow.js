import React from 'react';
// Nothing on ProductCategoryRow
class ProductCategoryRow extends React.PureComponent {
  render() {
    const category = this.props.category;
    return (
      <tr>
        <th colSpan="2">{category}</th>
      </tr>
    );
  }
}

export default ProductCategoryRow;
