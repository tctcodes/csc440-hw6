import React from 'react';
import PropTypes from 'prop-types';
import ProductCategoryRow from './ProductCategoryRow';
import ProductRow from './ProductRow';

class ProductTable extends React.PureComponent {
  render() {
    const rows = [];
    let lastCategory = null;

    this.props.items.forEach(product => {
      if (product.name.indexOf(this.props.filterText) === -1) {
        return;
      }
      if (this.props.inStockOnly && !product.stocked) {
        return;
      }
      if (product.category !== lastCategory) {
        rows.push(
          <ProductCategoryRow
            category={product.category}
            key={product.category}
          />,
        );
      }
      rows.push(
        <ProductRow
          product={product}
          key={product.name}
          onClick={this.props.updateStock}
        />,
      );
      lastCategory = product.category;
    });

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

ProductTable.propTypes = {
  items: PropTypes.array,
  filterText: PropTypes.string,
  inStockOnly: PropTypes.bool,
  updateStock: PropTypes.func,
};

export default ProductTable;
