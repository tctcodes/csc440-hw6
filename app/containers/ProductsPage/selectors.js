/**
 * ProductsPage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectProducts = state => state.get('products', initialState);

const makeSelectItems = () =>
  createSelector(selectProducts, productState => productState.get('items'));

const makeSelectFilterText = () =>
  createSelector(selectProducts, productState =>
    productState.get('filterText'),
  );

const makeSelectInStockOnly = () =>
  createSelector(selectProducts, productState =>
    productState.get('inStockOnly'),
  );

export {
  selectProducts,
  makeSelectItems,
  makeSelectFilterText,
  makeSelectInStockOnly,
};
