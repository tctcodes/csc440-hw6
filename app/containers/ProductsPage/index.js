/**
 * ProductsPage
 *
 * Lists Products for the store.
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { PropTypes } from 'prop-types';
import injectReducer from 'utils/injectReducer';
import {
  handleFilterText,
  handleStockStatus,
  addNewProduct,
  updateStock,
} from './actions';
import reducer from './reducer';

import SearchBar from './SearchBar';
import ProductTable from './ProductTable';
import NewProductForm from './NewProductForm';

/* eslint-disable react/prefer-stateless-function */
export class ProductsPage extends React.Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Products Page</title>
          <meta
            name="description"
            content="Products page for React Boilerplate"
          />
        </Helmet>
        <h1>Hello World</h1>
        <div>
          <NewProductForm addNewProduct={this.props.addNewProduct} />
          <SearchBar
            handleFilterText={this.props.handleFilterText}
            handleStockStatus={this.props.handleStockStatus}
            inStockOnly={this.props.inStockOnly}
          />
          <ProductTable
            items={this.props.items}
            filterText={this.props.filterText}
            inStockOnly={this.props.inStockOnly}
            updateStock={this.props.updateStock}
          />
        </div>
      </div>
    );
  }
}

ProductsPage.propTypes = {
  items: PropTypes.array,
  inStockOnly: PropTypes.bool,
  filterText: PropTypes.string,
  handleFilterText: PropTypes.func,
  handleStockStatus: PropTypes.func,
  addNewProduct: PropTypes.func,
  updateStock: PropTypes.func,
};

const mapDispatchToProps = {
  handleFilterText,
  handleStockStatus,
  addNewProduct,
  updateStock,
};

function mapStateToProps(state) {
  return {
    items: state.get('products').items,
    filterText: state.get('products').filterText,
    inStockOnly: state.get('products').inStockOnly,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'products', reducer });

export default compose(
  withReducer,
  withConnect,
)(ProductsPage);
