// import { fromJS } from 'immutable';
// import { stockList } from './stockSheet';
import {
  HANDLE_FILTER_TEXT,
  HANDLE_STOCK_STATUS,
  ADD_NEW_PRODUCT,
  UPDATE_STOCK,
} from './constants';

// Initial state of the products page.
export const initialState = {
  items: [
    {
      category: 'Sporting Goods',
      price: '$49.99',
      stocked: true,
      name: 'Football',
    },
    {
      category: 'Sporting Goods',
      price: '$9.99',
      stocked: true,
      name: 'Baseball',
    },
    {
      category: 'Sporting Goods',
      price: '$29.99',
      stocked: false,
      name: 'Basketball',
    },
    {
      category: 'Electronics',
      price: '$99.99',
      stocked: true,
      name: 'iPod Touch',
    },
    {
      category: 'Electronics',
      price: '$399.99',
      stocked: false,
      name: 'iPhone 5',
    },
    {
      category: 'Electronics',
      price: '$199.99',
      stocked: true,
      name: 'Nexus 7',
    },
  ],
  filterText: '',
  inStockOnly: false,
};

function productsReducer(state = initialState, action) {
  switch (action.type) {
    case HANDLE_FILTER_TEXT:
      return {
        ...state,
        filterText: action.payload,
      };
    case HANDLE_STOCK_STATUS:
      return {
        ...state,
        inStockOnly: action.payload,
      };
    case ADD_NEW_PRODUCT:
      return {
        ...state,
        items: [...state.items, action.payload],
      };
    case UPDATE_STOCK:
      return {
        ...state,
        items: state.items.map(
          item =>
            action.payload === item.name
              ? { ...item, stocked: !item.stocked }
              : item,
        ),
      };
    default:
      return state;
  }
}

export default productsReducer;
